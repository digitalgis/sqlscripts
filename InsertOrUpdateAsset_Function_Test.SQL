SELECT datahub_target.InsertOrUpdateAsset('<DataArea>
		<Sync>
			<AccountingEntityID>1000</AccountingEntityID>
			<LocationID>S_1000</LocationID>
			<ActionCriteria>
				<ActionExpression actionCode="Change" />
				<ChangeStatus>
					<EffectiveDateTime>2019-09-26T02:47:16</EffectiveDateTime>
				</ChangeStatus>
			</ActionCriteria>
		</Sync>
		<AssetMaster>
			<IDs>
				<ID lid="lid://infor.ips.ips" accountingEntity="1000" variationID="5">157762</ID>
			</IDs>
			<LastModificationDateTime>2019-09-26T14:47:12</LastModificationDateTime>
			<LastModificationPerson>fd46a58fbb6f44a8af1</LastModificationPerson>
			<DocumentDateTime>2019-09-23T14:49:21</DocumentDateTime>
			<Description>Tank,Sludge Chamber No.7</Description>
			<Note>Tank,Sludge Chamber No.4</Note>
			<DocumentReference>
				<DocumentID>
					<ID>489169</ID>
				</DocumentID>
			</DocumentReference>
			<UserArea>
				<Property>
					<NameValue name="AddedBy" type="String">8f16beefe1c3433d9b9</NameValue>
				</Property>
				<Property>
					<NameValue name="AddressQualifier" type="String">STWKO (Waikato WTP) - Clarification - Sludge Processing - Tank 24</NameValue>
				</Property>
				<Property>
					<NameValue name="AlternateId" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="Area" type="String">STWKO</NameValue>
				</Property>
				<Property>
					<NameValue name="BuildingFloor" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="BuildingRoom" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="Complex" type="Integer">86186</NameValue>
				</Property>
				<Property>
					<NameValue name="ComplexID" type="String">STWKO</NameValue>
				</Property>
				<Property>
					<NameValue name="AssetType" type="String">Containment Structure</NameValue>
				</Property>
				<Property>
					<NameValue name="AssetTypeCode" type="String">1016</NameValue>
				</Property>
				<Property>
					<NameValue name="District" type="String">SLUDGE</NameValue>
				</Property>
				<Property>
					<NameValue name="ExpiredBy" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="ExpireDate" type="DateTimeType">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="Intersection" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="Location" type="String">TK24</NameValue>
				</Property>
				<Property>
					<NameValue name="MainLine" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitIDAsset" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitID2Asset" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="MapNumber" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitIDTypeAssetType" type="String">1</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitIDType" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitID2TypeAssetType" type="String">1</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitID2Type" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="ParallelLineNumber" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="Parcel" type="Integer">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="RailSegment" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="StreetSegment" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="Site" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="ServiceLine" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="SubArea" type="String">W52</NameValue>
				</Property>
				<Property>
					<NameValue name="ID" type="String">157762</NameValue>
				</Property>
				<Property>
					<NameValue name="ID2" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="UnitType" type="String">TNK-UNDEF</NameValue>
				</Property>
				<Property>
					<NameValue name="UsageArea" type="Integer">97832</NameValue>
				</Property>
				<Property>
					<NameValue name="UsageAreaID" type="String">STWKO-52-SLUDGE-TK24</NameValue>
				</Property>
				<Property>
					<NameValue name="RailCrossing" type="Integer">1</NameValue>
				</Property>
				<Property>
					<NameValue name="Costcentre" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="CostcentreDesc" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="Department" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="Criticality" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="SafetyCritical" type="String">N</NameValue>
				</Property>
				<Property>
					<NameValue name="AssetClass" type="String">CONT</NameValue>
				</Property>
				<Property>
					<NameValue name="ProjectNo" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="ProjectWise" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="DrawingNo" type="String">
					</NameValue>
				</Property>
				<Property>
					<NameValue name="DrawingNoRef" type="String">
					</NameValue>
				</Property>
				<SimpleAgencyAsset>
					<Property>
						<NameValue name="XCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="YCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="ZCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="Ownership" type="String">WSL-WS</NameValue>
					</Property>
					<Property>
						<NameValue name="COMMISSION_DATE" type="DateTimeType">2011-11-15T00:00:00</NameValue>
					</Property>
					<Property>
						<NameValue name="EQUIPMENTNO" type="String">STWKO-52-TK-24</NameValue>
					</Property>
					<Property>
						<NameValue name="EQUIPMENTID" type="Integer">10078157</NameValue>
					</Property>
					<Property>
						<NameValue name="Manufacturer" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Manufactured" type="DateTimeType">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Purchased" type="DateTimeType">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="PurchaseCost" type="Decimal">0.0</NameValue>
					</Property>
					<Property>
						<NameValue name="ModelNumber" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="SerialNumber" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Color" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Surface" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Condition" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Material" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="ExpectedLife" type="Short">0</NameValue>
					</Property>
					<Property>
						<NameValue name="ExpectedLifeUOM" type="String">None</NameValue>
					</Property>
					<Property>
						<NameValue name="Length" type="Double">0</NameValue>
					</Property>
					<Property>
						<NameValue name="LengthUOM" type="String">None</NameValue>
					</Property>
					<Property>
						<NameValue name="AssetTypeCode" type="String">TNK</NameValue>
					</Property>
					<Property>
						<NameValue name="AssetSubTypeCode" type="String">UNDEF</NameValue>
					</Property>
					<Property>
						<NameValue name="AssetClass" type="String">CONT</NameValue>
					</Property>
					<Property>
						<NameValue name="Service Status" type="String">OP</NameValue>
					</Property>
					<Property>
						<NameValue name="Department" type="String">WSS</NameValue>
					</Property>
					<Property>
						<NameValue name="Costcentre" type="String">2211</NameValue>
					</Property>
					<Property>
						<NameValue name="Criticality" type="String">2</NameValue>
					</Property>
					<Property>
						<NameValue name="CostcentreDesc" type="String">Waikato WTP</NameValue>
					</Property>
					<Property>
						<NameValue name="SafetyCritical" type="String">N</NameValue>
					</Property>
					<Property>
						<NameValue name="SizeDiameter" type="String">0</NameValue>
					</Property>
					<Property>
						<NameValue name="SizeUOM" type="String">None</NameValue>
					</Property>
					<Property>
						<NameValue name="ProjectNo" type="String">78546</NameValue>
					</Property>
					<Property>
						<NameValue name="ProjectWise" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="DrawingNo" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="DrawingNoRef" type="String">
						</NameValue>
					</Property>
				</SimpleAgencyAsset>
				<ComplexAssetAttributes>
					<Property>
						<NameValue name="Service Status" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="XCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="YCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="ZCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="COMMISSION_DATE" type="DateTimeType">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Ownership" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Area" type="String">Area</NameValue>
					</Property>
					<Property>
						<NameValue name="Department" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Costcentre" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="CostcentreDesc" type="String">
						</NameValue>
					</Property>
				</ComplexAssetAttributes>
				<UsageAreaAssetAttributes>
					<Property>
						<NameValue name="XCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="YCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="ZCoordinate" type="Decimal">0</NameValue>
					</Property>
					<Property>
						<NameValue name="COMMISSION_DATE" type="DateTimeType">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Ownership" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Department" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Service Status" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Costcentre" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="Criticality" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="CostcentreDesc" type="String">
						</NameValue>
					</Property>
					<Property>
						<NameValue name="SafetyCritical" type="String">N</NameValue>
					</Property>
				</UsageAreaAssetAttributes>
				<PlantEquipment>
					<Ownership>
					</Ownership>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<ManufacturedDate>
					</ManufacturedDate>
				</PlantEquipment>
				<PlantMiscellaneous>
					<Ownership>
					</Ownership>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<ManufacturedDate>
					</ManufacturedDate>
					<Color>
					</Color>
					<Condition>
					</Condition>
					<Length>0</Length>
					<LengthUOM>
					</LengthUOM>
					<Material>
					</Material>
					<SurfaceCover>
					</SurfaceCover>
					<Diameter>
					</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</PlantMiscellaneous>
				<FleetVehicle>
					<Ownership>
					</Ownership>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<ManufacturedDate>
					</ManufacturedDate>
				</FleetVehicle>
				<SewerBackflowPreventer>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<Diameter>
					</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</SewerBackflowPreventer>
				<SewerMain>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<Manufacturer>
					</Manufacturer>
					<PipeLength>0</PipeLength>
					<DiameterUOM>
					</DiameterUOM>
					<Diameter>
					</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</SewerMain>
				<SewerManhole>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<Diameter>
					</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</SewerManhole>
				<SewerMiscellaneous>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<Manufacturer>
					</Manufacturer>
					<ModelNumber>
					</ModelNumber>
					<Color>
					</Color>
					<SurfaceCover>
					</SurfaceCover>
					<Condition>
					</Condition>
					<Material>
					</Material>
					<Length>0</Length>
					<LengthUOM>
					</LengthUOM>
					<Diameter>0</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</SewerMiscellaneous>
				<SewerLiftStation>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
				</SewerLiftStation>
				<SewerServiceLine>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<Manufacturer>
					</Manufacturer>
					<SurfaceCover>
					</SurfaceCover>
					<PipeMaterial>
					</PipeMaterial>
					<Length>0</Length>
					<LengthUOM>
					</LengthUOM>
					<Diameter>0</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</SewerServiceLine>
				<SewerValve>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<ManufacturedDate>
					</ManufacturedDate>
					<Size>0</Size>
					<SizeUOM>
					</SizeUOM>
				</SewerValve>
				<WaterBackflowPreventer>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<Size>0</Size>
					<SizeUOM>
					</SizeUOM>
				</WaterBackflowPreventer>
				<WaterHydrant>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<BarrelSize>0</BarrelSize>
					<BarrelSizeUOM>
					</BarrelSizeUOM>
				</WaterHydrant>
				<WaterMain>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<Manufacturer>
					</Manufacturer>
					<SurfaceCover>
					</SurfaceCover>
					<PipeMaterial>
					</PipeMaterial>
					<Length>0</Length>
					<LengthUOM>
					</LengthUOM>
					<Diameter>0</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</WaterMain>
				<WaterMeter>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<Manufacturer>
					</Manufacturer>
					<ManufacturedDate>
					</ManufacturedDate>
					<ModelNumber>
					</ModelNumber>
					<Size>0</Size>
					<SizeUOM>
					</SizeUOM>
				</WaterMeter>
				<WaterMiscellaneous>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
					<Color>
					</Color>
					<SurfaceCover>
					</SurfaceCover>
					<Condition>
					</Condition>
					<Material>
					</Material>
					<Length>0</Length>
					<LengthUOM>
					</LengthUOM>
					<Diameter>0</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</WaterMiscellaneous>
				<WaterPumpStation>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
				</WaterPumpStation>
				<WaterServiceLine>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<SurfaceCover>
					</SurfaceCover>
					<PipeMaterial>
					</PipeMaterial>
					<Length>0</Length>
					<LengthUOM>
					</LengthUOM>
					<Diameter>0</Diameter>
					<DiameterUOM>
					</DiameterUOM>
				</WaterServiceLine>
				<WaterValve>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Size>0</Size>
					<SizeUOM>
					</SizeUOM>
				</WaterValve>
				<WaterNode>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
				</WaterNode>
				<SewerNode>
					<YCoordinate>0</YCoordinate>
					<XCoordinate>0</XCoordinate>
					<ZCoordinate>0</ZCoordinate>
					<Ownership>
					</Ownership>
					<EQUIPMENTNO>
					</EQUIPMENTNO>
					<EQUIPMENTID>0</EQUIPMENTID>
					<ModelNumber>
					</ModelNumber>
					<Manufacturer>
					</Manufacturer>
				</SewerNode>
				<AssetValuations />
			</UserArea>
		</AssetMaster>
	</DataArea>'::xml);