-- FUNCTION: datahub_target.WSL_GIS_PostgreSql_Database_InsertOrUpdateAsset(xml)

-- DROP FUNCTION datahub_target.WSL_GIS_PostgreSql_Database_InsertOrUpdateAsset(xml);

CREATE OR REPLACE FUNCTION datahub_target.WSL_GIS_PostgreSql_Database_InsertOrUpdateAsset(
	xmlData xml)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    
AS $BODY$
DECLARE 
	
	-- Proc Update Asset in GIS and in postgresql for watercare data lake
	/*
	 *	This Procedure executes copy of update of Asset in GIS, when Asset which is added or changed in IPS
	 *
	 *	
	 *	=====================================================================================
	 *	S.No     Date				IssueNo						Desription
	 *  =====================================================================================
	 *	1.		22/10/2019			#						Asset IPS to Postgresql Integration
	 *  2.		19/11/2019			#						EleEleWaterValve, EleEleWaterPumpStation
	 *														does not exists, corrected
	 *  3.		23/11/2019			#						Rename the Function
	 *	=====================================================================================
	 */
	 
	-- Start of Declarations
	xmlNoun xml;
	
	Complex varchar(50) := 'Complex';
	UsageArea varchar(50) := 'Usage Area';
	PlantEquipment varchar(50) := 'Plant Equipment';
	PlantMiscellaneous varchar(50) := 'Plant Miscellaneous';
	Vehicle varchar(50) := 'Vehicle';
	SewerBackflow varchar(50) := 'Sewer Backflow';
	SewerMain varchar(50) := 'Sewer Main';
	SewerManhole varchar(50) := 'Sewer Manhole';
	SewerMiscellaneous varchar(50) := 'Sewer Miscellaneous';
	SewerLiftStation varchar(50) := 'Sewer Lift Station';
	SewerPump varchar(50) := 'Sewer Pump';
	SewerServiceLine varchar(50) := 'Sewer Service Line';
	SewerValve varchar(50) := 'Sewer Valve';
	SewerNode varchar(50) := 'Sewer Node';
	WaterBackflow varchar(50) := 'Water Backflow';
	WaterHydrant varchar(50) := 'Water Hydrant';
	WaterMain varchar(50) := 'Water Main';
	WaterMeter varchar(50) := 'Water Meter';
	WaterMiscellaneous varchar(50) := 'Water Miscellaneous';
	WaterPumpStation varchar(50) := 'Water Pump Station';
	WaterServiceLine varchar(50) := 'Water Service Line';
	WaterValve varchar(50) := 'Water Valve';
	WaterNode varchar(50) := 'Water Node';

	AssetTypeDesc varchar(50);
	ElementPathName varchar(75);
	xmlcompkey varchar(25);
	
	ErrMsg varchar(200);
	
	EleComplexAssetAttributes varchar(100) := 'ComplexAssetAttributes';
	EleUsageAreaAssetAttributes varchar(100) := 'UsageAreaAssetAttributes';
	ElePlantEquipment varchar(100) := 'PlantEquipment';
	ElePlantMiscellaneous varchar(100) := 'PlantMiscellaneous';
	EleFleetVehicle varchar(100) := 'FleetVehicle';
	EleSewerBackflowPreventer varchar(100) := 'SewerBackflowPreventer';
	EleSewerMain varchar(100) := 'SewerMain';
	EleSewerManhole varchar(100) := 'SewerManhole';
	EleSewerMiscellaneous varchar(100) := 'SewerMiscellaneous';
	EleSewerLiftStation varchar(100) := 'SewerLiftStation';
	EleSewerPump varchar(100) := 'SewerPump';
	EleSewerServiceLine varchar(100) := 'SewerServiceLine';
	EleSewerValve varchar(100) := 'SewerValve';
	EleSewerNode varchar(100) := 'SewerNode';
	EleWaterBackflowPreventer varchar(100) := 'WaterBackflowPreventer';
	EleWaterHydrant varchar(100) := 'WaterHydrant';
	EleWaterMain varchar(100) := 'WaterMain';
	EleWaterMeter varchar(100) := 'WaterMeter';
	EleWaterMiscellaneous varchar(100) := 'WaterMiscellaneous';
	EleWaterPumpStation varchar(100) := 'WaterPumpStation';
	EleWaterServiceLine varchar(100) := 'WaterServiceLine';
	EleWaterValve varchar(100) := 'WaterValve';
	EleWaterNode varchar(100) := 'WaterNode';
	EleSimpleAgencyAsset varchar(100) := 'SimpleAgencyAsset';
	
	--Table column declarations
	tblcompkey integer;
    tblfac_code character varying(10);
    tblfac_desc character varying(50);
    tblcomptype integer;
    tblcompcode character varying(20);
    tblmaincomp1 integer;
    tblmaincomp2 integer;
    tblunittype character varying(10);
    tblunitdesc character varying(80);
    tblunitid character varying(16);
    tblunitid2 character varying(16);
    tblprocess character varying(15);
    tblsystem character varying(10);
    tblloccode character varying(10);
    tblposition character varying(14);
    tblstatus character varying(10);
    tblmaterial character varying(10);
    tblnom_dia_mm smallint;
    tbllength double precision;
    tblowner character varying(10);
    tblinstalled timestamp(6);
    tbladdby character varying(30);
    tbladddttm timestamp(3);
    tblmodby character varying(30);
    tblmoddttm timestamp(3);
    tbladdrkey integer;
    tblhierarchy integer;
    tblhierarchyid character varying(50);
    tblprojectno character varying(20);
    tbldmsref character varying(20);
    tblasbuilt character varying(20);
    tblasbuiltref character varying(20);
    tblcpsiteid character varying(20);
		
	-- End of Declarations
	

BEGIN 
	-- START OF PROC
	
	--Get Noun XML
	--xmlNoun := (xpath('DataArea/AssetMaster', xmlData))[1];
	xmlNoun := xmlData;
	--RAISE NOTICE 'Input Asset %', xmlNoun;
	xmlcompkey := unnest(xpath('//IDs/ID/text()', xmlNoun));
	AssetTypeDesc := unnest(xpath('//UserArea/Property/NameValue[@name=''AssetType'']/text()', xmlNoun));
	--RAISE NOTICE 'Input Asset compkey % and AssetTypeDesc %', xmlcompkey,AssetTypeDesc;
	--RAISE NOTICE '%',xmlcompkey;
	
	--Verify the Asset PK Exists
	IF xmlcompkey is null OR xmlcompkey = '' THEN	
		--Raise Error Asset does not exists
		ErrMsg := 'GIS Asset ' || cast(xmlcompkey as varchar) || ' insert failed';
		RAISE EXCEPTION 'Invalid Asset compkey in XML : %', ErrMsg;
		RETURN NULL;
	END IF;

	CASE   
		WHEN  AssetTypeDesc = Complex THEN ElementPathName := EleComplexAssetAttributes;
		WHEN  AssetTypeDesc = UsageArea THEN ElementPathName := EleUsageAreaAssetAttributes;
		WHEN  AssetTypeDesc = PlantEquipment THEN ElementPathName := ElePlantEquipment;
		WHEN  AssetTypeDesc = PlantMiscellaneous THEN ElementPathName := ElePlantMiscellaneous;
		WHEN  AssetTypeDesc = Vehicle THEN ElementPathName := EleFleetVehicle;
		WHEN  AssetTypeDesc = SewerBackflow THEN ElementPathName := EleSewerBackflowPreventer;
		WHEN  AssetTypeDesc = SewerMain THEN ElementPathName := EleSewerMain;
		WHEN  AssetTypeDesc = SewerManhole THEN ElementPathName := EleSewerManhole;
		WHEN  AssetTypeDesc = SewerMiscellaneous THEN ElementPathName := EleSewerMiscellaneous;
		WHEN  AssetTypeDesc = SewerLiftStation THEN ElementPathName := EleSewerLiftStation;
		WHEN  AssetTypeDesc = SewerPump THEN ElementPathName := EleSewerPump;
		WHEN  AssetTypeDesc = SewerServiceLine THEN ElementPathName := EleSewerServiceLine;
		WHEN  AssetTypeDesc = SewerValve  THEN ElementPathName := EleSewerValve;
		WHEN  AssetTypeDesc = SewerNode  THEN ElementPathName := EleSewerNode;
		WHEN  AssetTypeDesc = WaterBackflow  THEN ElementPathName := EleWaterBackflowPreventer;
		WHEN  AssetTypeDesc = WaterHydrant  THEN ElementPathName := EleWaterHydrant;
		WHEN  AssetTypeDesc = WaterMain  THEN ElementPathName := EleWaterMain;
		WHEN  AssetTypeDesc = WaterMeter  THEN ElementPathName := EleWaterMeter;
		WHEN  AssetTypeDesc = WaterMiscellaneous  THEN ElementPathName := EleWaterMiscellaneous;
		WHEN  AssetTypeDesc = WaterPumpStation  THEN ElementPathName := EleWaterPumpStation;
		WHEN  AssetTypeDesc = WaterServiceLine  THEN ElementPathName := EleWaterServiceLine;
		WHEN  AssetTypeDesc = WaterValve  THEN ElementPathName := EleWaterValve;
		WHEN  AssetTypeDesc = WaterNode  THEN ElementPathName := EleWaterNode;
        ELSE ElementPathName := EleSimpleAgencyAsset;
	END CASE;
	--RAISE NOTICE 'Element Path Name: % AS %',AssetTypeDesc,ElementPathName;
	
	
    -- Select values
	SELECT CAST (CAST (unnest(xpath('//IDs/ID/text()', xmlNoun)) AS varchar) AS integer) AS compkey,
		CAST (CAST (unnest(xpath('(//UserArea/Property/NameValue[@name=''AssetTypeCode''])/text()', xmlNoun)) AS varchar)AS integer) AS comptype,
		CAST (CAST (unnest(xpath('//LastModificationDateTime/text()', xmlNoun)) AS varchar)AS timestamp) AS moddttm,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//LastModificationPerson/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS modby,
		CAST( CAST (unnest(xpath('//DocumentDateTime/text()', xmlNoun)) AS varchar)AS timestamp)  AS adddttm,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//Description/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'')  AS unitdesc,
		CAST (CAST (unnest(xpath('//DocumentReference/DocumentID/ID/text()', xmlNoun)) AS varchar) AS integer) AS addrkey,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''AddedBy'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'')  AS addby,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''Area'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'')  AS fac_code,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''District'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS "system",
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''Location'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS loccode,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''MapNumber'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS "position",
		CAST (CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''UnitIDTypeAssetType'']/text()', xmlNoun)) AS varchar) AS integer) AS maincomp1,
		CAST (CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''UnitID2TypeAssetType'']/text()', xmlNoun)) AS varchar) AS integer) AS maincomp2,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''SubArea'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS process,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''ID'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS unitid,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''ID2'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS unitid2,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''UnitType'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS unittype,
		CAST (CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''UsageArea'']/text()', xmlNoun)) AS varchar) AS integer) AS hierarchy,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''UsageAreaID'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS hierarchyid,
		NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''AssetClass'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS compcode,
		(SELECT NULLIF(TRIM(projectno),'')  FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''ProjectNo'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS projectno 
				UNION		
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/SimpleAgencyAsset/Property/NameValue[@name=''ProjectNo'']/text()', xmlNoun))AS varchar),chr(10),''),chr(9),'')),'') AS projectno 
			) AS project ORDER BY projectno ASC LIMIT 1) AS projectno,
		(SELECT NULLIF(TRIM(dmsref),'')  FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''ProjectWise'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS dmsref
				UNION		
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/SimpleAgencyAsset/Property/NameValue[@name=''ProjectWise'']/text()', xmlNoun))AS varchar),chr(10),''),chr(9),'')),'') AS dmsref
			) AS project ORDER BY dmsref ASC LIMIT 1) AS dmsref,
		(SELECT NULLIF(TRIM(asbuilt),'')  FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''DrawingNo'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS asbuilt
				UNION		
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/SimpleAgencyAsset/Property/NameValue[@name=''DrawingNo'']/text()', xmlNoun))AS varchar),chr(10),''),chr(9),'')),'') AS asbuilt
			) AS project ORDER BY asbuilt ASC LIMIT 1) AS asbuilt,
		(SELECT NULLIF(TRIM(asbuiltref),'')  FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/Property/NameValue[@name=''DrawingNoRef'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS asbuiltref
				UNION		
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/SimpleAgencyAsset/Property/NameValue[@name=''DrawingNoRef'']/text()', xmlNoun))AS varchar),chr(10),''),chr(9),'')),'') AS asbuiltref
			) AS project ORDER BY asbuiltref ASC LIMIT 1) AS asbuiltref,	
		(SELECT NULLIF(TRIM("owner"),'') FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Property/NameValue[@name=''Ownership'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS "owner"
				WHERE ElementPathName IN (EleSimpleAgencyAsset, EleComplexAssetAttributes, EleUsageAreaAssetAttributes)
				UNION 
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Ownership/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS "owner"
				WHERE ElementPathName IN (ElePlantEquipment,ElePlantMiscellaneous,EleFleetVehicle, EleSewerBackflowPreventer,
										EleSewerMain, EleSewerManhole, EleSewerMiscellaneous, EleSewerLiftStation, 
										EleSewerPump, EleSewerServiceLine, EleSewerValve, EleSewerNode, EleWaterBackflowPreventer, 
										EleWaterHydrant, EleWaterMain, EleWaterMeter, EleWaterMiscellaneous, EleWaterPumpStation, 
										EleWaterServiceLine, EleWaterValve, EleWaterNode)
			 ) AS own ORDER BY "owner" ASC LIMIT 1) AS "owner",
		(SELECT installed FROM 
			(SELECT CAST (NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Property/NameValue[@name=''COMMISSION_DATE'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS timestamp) AS installed
				WHERE ElementPathName IN (EleSimpleAgencyAsset, EleComplexAssetAttributes, EleUsageAreaAssetAttributes)
				UNION 
			SELECT CAST (NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/COMMISSION_DATE/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS timestamp) AS installed
				WHERE ElementPathName IN (ElePlantEquipment,ElePlantMiscellaneous,EleFleetVehicle, EleSewerBackflowPreventer,
										EleSewerMain, EleSewerManhole, EleSewerMiscellaneous, EleSewerLiftStation, 
										EleSewerPump, EleSewerServiceLine, EleSewerValve, EleSewerNode, EleWaterBackflowPreventer, 
										EleWaterHydrant, EleWaterMain, EleWaterMeter, EleWaterMiscellaneous, EleWaterPumpStation, 
										EleWaterServiceLine, EleWaterValve, EleWaterNode)
			 ) AS ins ORDER BY installed ASC LIMIT 1) AS installed,		
		(SELECT NULLIF(TRIM(status),'') FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Property/NameValue[@name=''Service Status'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS status
				WHERE ElementPathName IN (EleSimpleAgencyAsset, EleComplexAssetAttributes, EleUsageAreaAssetAttributes)
				UNION 
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/ServiceStatus/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS status
				WHERE ElementPathName IN (ElePlantEquipment,ElePlantMiscellaneous,EleFleetVehicle, EleSewerBackflowPreventer,
										EleSewerMain, EleSewerManhole, EleSewerMiscellaneous, EleSewerLiftStation, 
										EleSewerPump, EleSewerServiceLine, EleSewerValve, EleSewerNode, EleWaterBackflowPreventer, 
										EleWaterHydrant, EleWaterMain, EleWaterMeter, EleWaterMiscellaneous, EleWaterPumpStation, 
										EleWaterServiceLine, EleWaterValve, EleWaterNode)
			 ) AS sts ORDER BY status ASC LIMIT 1) AS status,
		(SELECT NULLIF(TRIM(material),'') FROM 
			(SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Property/NameValue[@name=''Material'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS material
				WHERE ElementPathName IN (EleSimpleAgencyAsset, EleComplexAssetAttributes, EleUsageAreaAssetAttributes)
				UNION 
			SELECT NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Material/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS material
				WHERE ElementPathName IN (ElePlantEquipment,ElePlantMiscellaneous,EleFleetVehicle, EleSewerBackflowPreventer,
										EleSewerMain, EleSewerManhole, EleSewerMiscellaneous, EleSewerLiftStation, 
										EleSewerPump, EleSewerServiceLine, EleSewerValve, EleSewerNode, EleWaterBackflowPreventer, 
										EleWaterHydrant, EleWaterMain, EleWaterMeter, EleWaterMiscellaneous, EleWaterPumpStation, 
										EleWaterServiceLine, EleWaterValve, EleWaterNode)
			 ) AS mat ORDER BY material ASC LIMIT 1) AS material,
		(SELECT "length" FROM 
			(SELECT CAST(NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Property/NameValue[@name=''Length'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'')AS decimal) AS "length"
				WHERE ElementPathName IN (EleSimpleAgencyAsset, EleComplexAssetAttributes, EleUsageAreaAssetAttributes)
				UNION 
			SELECT CAST(NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Length/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS decimal) AS "length"
				WHERE ElementPathName IN (ElePlantEquipment,ElePlantMiscellaneous,EleFleetVehicle, EleSewerBackflowPreventer,
										EleSewerMain, EleSewerManhole, EleSewerMiscellaneous, EleSewerLiftStation, 
										EleSewerPump, EleSewerServiceLine, EleSewerValve, EleSewerNode, EleWaterBackflowPreventer, 
										EleWaterHydrant, EleWaterMain, EleWaterMeter, EleWaterMiscellaneous, EleWaterPumpStation, 
										EleWaterServiceLine, EleWaterValve, EleWaterNode)
			 ) AS lenn ORDER BY "length" ASC LIMIT 1) AS "length",		
		(SELECT nom_dia_mm FROM 
			(SELECT CAST(NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Property/NameValue[@name=''SizeDiameter'']/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS smallint) AS nom_dia_mm
				WHERE ElementPathName IN (EleSimpleAgencyAsset, EleComplexAssetAttributes, EleUsageAreaAssetAttributes)
				UNION 
			SELECT CAST(NULLIF(TRIM(REPLACE(REPLACE(CAST (unnest(xpath('//UserArea/'||ElementPathName||'/Diameter/text()', xmlNoun)) AS varchar),chr(10),''),chr(9),'')),'') AS smallint) AS nom_dia_mm
				WHERE ElementPathName IN (ElePlantEquipment,ElePlantMiscellaneous,EleFleetVehicle, EleSewerBackflowPreventer,
										EleSewerMain, EleSewerManhole, EleSewerMiscellaneous, EleSewerLiftStation, 
										EleSewerPump, EleSewerServiceLine, EleSewerValve, EleSewerNode, EleWaterBackflowPreventer, 
										EleWaterHydrant, EleWaterMain, EleWaterMeter, EleWaterMiscellaneous, EleWaterPumpStation, 
										EleWaterServiceLine, EleWaterValve, EleWaterNode)
			) AS nmm ORDER BY nom_dia_mm ASC LIMIT 1) AS nom_dia_mm 
		INTO tblcompkey, tblcomptype, tblmoddttm, tblmodby,
			tbladddttm, tblunitdesc, tbladdrkey, tbladdby, tblfac_code, tblsystem, tblloccode, tblposition, 
			tblmaincomp1, tblmaincomp2, tblprocess, tblunitid, tblunitid2, tblunittype, tblhierarchy, 
			tblhierarchyid, tblcompcode, tblprojectno, tbldmsref, tblasbuilt, tblasbuiltref,
			tblowner, tblinstalled,	tblstatus, tblmaterial,tbllength, tblnom_dia_mm;	

	-- UPSERT Statment
	IF NOT EXISTS(SELECT compkey FROM datahub_target.ips_assets WHERE  compkey = tblcompkey) THEN
		INSERT INTO datahub_target.ips_assets (compkey, comptype, moddttm, modby,
			adddttm, unitdesc, addrkey, addby, fac_code, "system", loccode, "position", 
			maincomp1, maincomp2, process, unitid, unitid2, unittype, "hierarchy", 
			hierarchyid, compcode, projectno, dmsref, asbuilt, asbuiltref,
			"owner", installed,	status, material,"length", nom_dia_mm
		)
		SELECT tblcompkey, tblcomptype, tblmoddttm, tblmodby,
			tbladddttm, tblunitdesc, tbladdrkey, tbladdby, tblfac_code, tblsystem, tblloccode, tblposition, 
			tblmaincomp1, tblmaincomp2, tblprocess, tblunitid, tblunitid2, tblunittype, tblhierarchy, 
			tblhierarchyid, tblcompcode, tblprojectno, tbldmsref, tblasbuilt, tblasbuiltref,
			tblowner, tblinstalled,	tblstatus, tblmaterial,tbllength, tblnom_dia_mm;
	ELSE	
		UPDATE datahub_target.ips_assets SET	
			comptype = tblcomptype, moddttm = tblmoddttm, modby = tblmodby,adddttm = tbladddttm, unitdesc = tblunitdesc, 
			addrkey = tbladdrkey, addby = tbladdby, fac_code = tblfac_code, "system" = tblsystem, loccode = tblloccode, 
			"position" = tblposition, maincomp1 = tblmaincomp1, maincomp2 = tblmaincomp2, process = tblprocess, 
			unitid = tblunitid, unitid2 = tblunitid2, unittype = tblunittype, "hierarchy" = tblhierarchy, 
			hierarchyid = tblhierarchyid, compcode = tblcompcode, projectno = tblprojectno, dmsref = tbldmsref, 
			asbuilt = tblasbuilt, asbuiltref = tblasbuiltref,"owner" = tblowner, installed = tblinstalled, 
			status = tblstatus, material = tblmaterial, "length" = tbllength, nom_dia_mm = tblnom_dia_mm 
			WHERE compkey = tblcompkey; 
	END IF;		
	
	RETURN NULL;
-- END OF PROC
END;$BODY$;
