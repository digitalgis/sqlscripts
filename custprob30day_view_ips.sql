USE [GISAdmin]

SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [GISWSL].[HA_CustProb30day_View_IPS]

AS
SELECT
	CAST (SERVNO AS int) as ID,
	CAST (SERVNO AS int) as SERVNO,
	CAST (ADDRKEY AS int) as ADDRKEY,
	CAST (REQUESTTYPE AS varchar(5)) as PROB,
	CAST (DESCRIPTION AS varchar(254)) as PROBDESC,
	CAST ('PROBDESC_SHORT' as varchar(50)) as PROBDESC_SHORT,
	RIGHT (CAST(PRIORITY AS varchar(4)), 1) as PRI,
	CAST (Dateadd(hh,datediff(hour,getdate(),getutcdate()),REPORTEDDTTM) AS datetime) as PROBDTTM,
	CAST (REPORTEDDTTM AS datetime) as PROBDTTM_HAN,
	CAST (NULL as varchar(5)) as RESCODE,
	CAST (NULL as datetime) as RESDTTM,
	CAST ((CASE
				when SERVAREA =  'Water' then 'WS'
				when SERVAREA =  'Waste' then 'WW'
				ELSE 'Other'
				END ) as varchar(6)) as NetWork,
	CAST ('1746478.52' as numeric(9,2)) as NZTM_E,
	CAST ('5912561.58' as numeric(9,2)) as NZTM_N

  FROM [GISDatahub].[GISWSL].[IP_SERVICEREQUEST]
