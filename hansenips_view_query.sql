DECLARE @Today DATE = GetDate()
DECLARE @Start DATE = DATEADD(month, -5, GETDATE())
DECLARE @End DATE = DATEADD(year, -1, GETDATE())

print @Today
print @Start
print @End

SELECT RES.SERVNO, RES.PROBDTTM, RES.AMS
FROM

(-- IPS Select Query
SELECT
      F.[SERVNO],
	  F.[PROBDTTM],
	  CAST ('IPS' AS varchar) as AMS

  FROM [HansenReporting].[IMSV7].[CUSTPROB] as F
  WHERE F.PROBDTTM between @Start and @Today

  UNION

-- Hansen Select Query
SELECT
      T.[SERVNO],
	  T.[PROBDTTM],
	  CAST ('Hansen' AS varchar) as AMS

  FROM [HansenReporting].[IMSV7].[CUSTPROB] as T
  WHERE T.PROBDTTM between @End and @Start) as RES

  ORDER BY RES.PROBDTTM DESC
