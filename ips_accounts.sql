CREATE TABLE datahub_target.ips_servicerequestlogs
(
    servno integer,
    custlogkey integer,
    logtype character varying(10) COLLATE pg_catalog."default",
    addby character varying(30) COLLATE pg_catalog."default",
    logfrdttm timestamp(6) without time zone,
    logtodttm timestamp(6) without time zone,
    comments text COLLATE pg_catalog."default",
    servnoa integer,
    servnob integer
)
