CREATE TABLE datahub_target.ips_servicerequest(
	LOADID INT GENERATED BY DEFAULT AS IDENTITY
        (START WITH 1 INCREMENT BY 1),
	SERVNO int NOT NULL,
	STATUS varchar (10) NOT NULL,
	SRSTATUS varchar (10) NULL,
	REQUESTTYPE varchar (10) NULL,
	DESCRIPTION varchar (50) NULL,
	PRIORITY varchar (50) NULL,
	REPORTEDDTTM Timestamp(6) NULL,
	ADDRKEY int NULL,
  FIRSTNAME varchar (50) NULL,
  MOBNUMA varchar (50) NULL,
  LASTNAME varchar (50) NULL,
  MOBNUMB varchar (50) NULL,
  WSLREP varchar (50) NULL,
  CREW varchar (50) NULL,
  SERVICEAREA varchar (50) NULL,
  CREWAREA varchar (50) NULL,
  SERVAREA varchar (50) NULL,
  INSPSTDTTM Timestamp(6) NULL,
  DUEDTTM Timestamp(6) NULL,
  CONTACTREQ varchar (50) NULL,
  RESDUEDTTM Timestamp(6) NULL,
  ASSETID varchar (50) NULL,
  FAULTDETAILS varchar (50) NULL,
  SIZEOFLEAK varchar (50) NULL,
  NEIGHBOURSAFFECTED varchar (50) NULL,
  ACCESSISSUES varchar (50) NULL,
  DIALYSISPATIENT varchar (50) NULL,
  KEYACCOUNT varchar (50) NULL,
  SCHEDULESITTREE varchar (50) NULL,
  TREE varchar (50) NULL,
  JOBCBD varchar (50) NULL,
  FAULTTYPE varchar (50) NULL,
  GISCOMPKEY varchar (50) NULL,
  PROPERTYDAMAGE varchar (50) NULL,
  THIRDPARTY varchar (50) NULL,
  SUPERVISOR varchar (50) NULL,
  HEALTHSAFETY varchar (50) NULL,
  PLANNEDMETER varchar (50) NULL,
  DIALYSISADDRESS varchar (100) NULL,
  KEYADDRESS varchar (100) NULL,
  PHOTOSPROVIDED varchar (50) NULL,
  PROJECTRELATED varchar (50) NULL,
  MULTIPLEPROPERTIES varchar (50) NULL,
  GATEVALVE varchar (50) NULL,
  TAPS varchar (50) NULL,
  FLUSH varchar (50) NULL,
  BOTTLEDWATER varchar (50) NULL,
  INTERNALDRAINAGE varchar (50) NULL,
  PROBELMSTART varchar (50) NULL,
  PLUMBERCONTACTED varchar (50) NULL,
  CCTV varchar (50) NULL,
  FREQUENCY varchar (50) NULL,
  PUBLICHEALTHRISK varchar (50) NULL,
  BANKACCNUM varchar (50) NULL,
  CUSTOUTCOME varchar (50) NULL

);
