DECLARE @Start DATE = DATEADD(month, Datediff(month, 0, GETDATE())-1,0)
DECLARE @End DATE = DATEADD(month, Datediff(month, -1, GETDATE())-1,-1)
/*
SELECT [SHAPE]
FROM [GISNet1].[GISWSL].[W_METER]
Where [INSTALLED] between @start and @end
SELECT [BZ_CODE], [BZ_NAME], [SHAPE]
FROM [GISNet2].[GISWSL].[W_SUPPLYZONE]
*/
Select [BZ_CODE], MIN([BZ_NAME]) as NAMED, COUNT (*) as COUNTS
From [GISNet2].[GISWSL].[W_SUPPLYZONE] as M
INNER JOIN
[GISNet1].[GISWSL].[W_METER] as MET
ON
M.[SHAPE].STIntersects(MET.[SHAPE])=1
WHERE MET.INSTALLED between @start and @end
GROUP BY[BZ_CODE]
